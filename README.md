# Letmeask

This project is a Q&A made whit React and using firebase as a service. Whit the letmeask app you can create a room about a topic and give the room code to people so they can go make questions, after that you can respond to people, you can mark the questions made as responded or removed. As a client you can surf in a room whithout make login, but to make a question you need to login whit your google account. After that you can make a like in others questions and make questions to the admin of the room.  

## Available Scripts

In the project directory, you can run:

### `initialize the app`

First things first. To use this app, download as a zip file and then open in your IDE. This app was made using Visual Studio Code. Besides IDE, you need node.js in your pc as well npm or yarn.

https://code.visualstudio.com/ -> Link for IDE vsCode

https://nodejs.org/en/         -> Link for node (v14.17.1)

This application uses firebase, so if you want to used it, create a new project in firebase using your google account after that create a .env.local in your root project and copy the configuration from firebase to the file created. 

### `npm i`

To install all the dependecies just put this command on the terminal in the root path of the downloaded file and will create the node_modules.

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

# Final Result
The final result can be test on this link https://letmeask-a7b9e.web.app/

![Alt-Text](src/assets/images/LetMeAsk.PNG)


